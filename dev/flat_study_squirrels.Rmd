---
title: "flat_minimal.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(glue)
pkgload::load_all()
```


```{r development, include=FALSE}
primary_fur_color="Cinnamon"

message(glue("We will focus on {primary_fur_color} squirrels"))
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# get_message_fur_color

Fonction pour donner la couleur à laquelle on s'interesse
    
```{r function-get_message_fur_color}
#' Title
#' 
#' Description
#' @param primary_fur_color character. Couleur a laquelle on s'interesse
#' 
#' @return message. Renvoie un message
#' 
#' @importFrom glue glue
#' 
#' @export
#' 
get_message_fur_color <- function(primary_fur_color){
  message(glue("We will focus on {primary_fur_color} squirrels"))
}
```
  
```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Black")
get_message_fur_color(primary_fur_color = "Cinnamon")
get_message_fur_color(primary_fur_color = "Blue")
```
  
```{r tests-get_message_fur_color}
test_that("get_message_fur_color works", {
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"), 
    regexp = "We will focus on Cinnamon squirrels"
  )
    expect_message(
    object = get_message_fur_color(primary_fur_color = "black"), 
    regexp = "We will focus on black squirrels"
  )
})
```

# Save an output as a csv file

You can save an output as a csv file with `save_as_csv()`. This function can be used, for instance, to save the filtered table created by `study_activity()`.
    
```{r function-save_as_csv}
#' Save an output as a csv file
#'
#' @param data Data frame. The table to be saved as a csv file.
#' @param path Character. Path where the csv file shloud be saved.
#' @param ... parametres de writecsv
#'
#' @importFrom assertthat assert_that not_empty has_extension is.dir is.writeable
#' @importFrom utils write.csv2
#' 
#' @return The path to the created csv file.
#' @export
#'
#' @examples
save_as_csv <- function(data,path,...){
  
  assert_that(is.data.frame(data))
  assert_that(not_empty(data))
  assert_that(has_extension(path, ext = "csv"))
  assert_that(is.dir(dirname(path)))
  assert_that(is.writeable(dirname(path)))
  
  dots <- list(...)
  write.csv2(x = data, file = path,...)
  
  return(invisible(path))
  
}
```

```{r example-save_as_csv}
# Create a temporary directory
my_temp_folder <- tempfile(pattern = "savecsv")
dir.create(my_temp_folder)

# Perform the analysis with study_activity()
res_activities_grat_squirrels <- study_activity(
  df_squirrels_act = data_act_squirrels, 
  col_primary_fur_color = "Gray"
  )

# Save res_activities_grat_squirrels$table as a csv
save_as_csv(res_activities_grat_squirrels$table, 
            path = file.path(my_temp_folder, "my_filtered_activities.csv"),
            na = "NA",
            fileEncoding = "UTF8",
            dec = ',',
            sep = ';')
file.path(my_temp_folder, "my_filtered_activities.csv")

# See if the csv exists
list.files(my_temp_folder)

# Read the csv
read.csv2(file.path(my_temp_folder, "my_filtered_activities.csv"))

# Delete the temporary folder
unlink(my_temp_folder, recursive = TRUE)
```
  
```{r tests-save_as_csv}
test_that("save_as_csv works", {
  
  # Everything is ok
  my_temp_folder <- tempfile(pattern = "savecsv")
  dir.create(my_temp_folder)

  expect_error(iris %>% save_as_csv(file.path(my_temp_folder, "output.csv")), 
              regexp = NA)
  
  expect_error(iris %>% save_as_csv(file.path(my_temp_folder, "output.csv")) %>% browseURL(), 
               regexp = NA)
  
  unlink(my_temp_folder, recursive = TRUE)
  
  # Error
  expect_error(iris %>% save_as_csv("output.xlsx"), 
               regexp = "does not have extension csv")
  
  expect_error(NULL %>% save_as_csv("output.csv"),
               regexp = "data is not a data frame")
  
  expect_error(iris %>% save_as_csv("does/no/exist.csv"), 
               regexp = "does not exist")
  
  expect_error(iris %>% save_as_csv("/usr/bin/output.csv"), 
               regexp = "not writeable")
  
})
```  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_study_squirrels.Rmd", vignette_name = "Study the squirrels")
```
